// index.js
// 获取应用实例
import { BaiduAPI, HuaweiAPI, AliyunAPI, TencentAPI } from '../../utils/apis/index';
import { getUUID } from '../../utils/util';
import { WEB_URL } from '../../constant';

const apis = {
  baidu: new BaiduAPI(),
  huawei: new HuaweiAPI(),
  aliyun: new AliyunAPI(),
  tencent: new TencentAPI(),
}
// const app = getApp();

const autoUploadKey = '__should_auto_upload__';
const noCropKey = '__no_crop__';

Page({
  data: {
    shouldAutoUpload: wx.getStorageSync(autoUploadKey) !== false,
    noCrop: wx.getStorageSync(noCropKey) !== false,
    state: 0,
    initFailed: false,
    tempPath: '',
    cropMaxWidth: 0,
    cropMaxHeight: 0,
    imagePath: '',
    resultText: '',
    result: null,
    baseUrl: 'https://yiyun.eki.space/api/yijian',
    apiValue: 0,
    apiTypes: [
      {
        key: 'auto', 
        title: '自动',
      },
      {
        key: 'huawei',
        title: '华为云',
      },
      {
        key: 'tencent',
        title: '腾讯云',
      },
      // {
      //   key: 'aliyun',
      //   title:  '阿里云',
      // },
      {
        key: 'baidu',
        title:  '百度云',
      }
    ]
  },
  async onLoad() {
    const { apiTypes } = this.data;
    const promises = apiTypes.map(async type => {
      if (type.key === 'auto') return type;
      try {
        if (await apis[type.key].init()) return type;
      } catch (error) {}
    });

    const res = await Promise.all(promises);
    const validTypes = [];
    const failedList = [];
    res.forEach((type, index) => {
      if (type) {
        validTypes.push(type);
      } else {
        failedList.push(apiTypes[index].title);
      }
    });

    if (failedList.length > 0) {
      wx.showModal({
        content: `初始化${failedList.join('、')}等API失败，请与作者联系！`,
        showCancel: false,
      });
      this.setData({
        apiTypes: validTypes,
      });
    }

    if (validTypes.length < 2) {
      this.setData({
        initFailed: true
      });
    }
  },
  // 拍照并调取 OCR 识别服务
  takePhoto () {
    const ctx = wx.createCameraContext();
    ctx.takePhoto({
      quality: 'high',
      success: res => {
        if (this.data.noCrop) {
          this.setData({
            imagePath: res.tempImagePath,
          }, this.readText);
        } else {
          this.setData({
            tempPath: res.tempImagePath,
          }, this.showSizer);
        }
      }
    });
  },
  async readText() {
    wx.showLoading({
      title: '正在识别 . . .',
      mask: true,
    });
    const { imagePath, apiTypes, apiValue, shouldAutoUpload } = this.data;
    const apiKey = apiTypes[apiValue].key;
    let triedApiCount = 0;
    let apiTypeIndex = apiKey === 'auto' ? 1 : apiTypes.indexOf(apiKey);
    let isSuccess = false;

    while (triedApiCount < apiTypes.length - 1) {
      const apiType = apiTypes[apiTypeIndex];
      const api = apis[apiType.key];
      try {
        const res = await api.read(imagePath);
        this.resolveData(res, apiKey);
        if (shouldAutoUpload) {
          this.sendToPC();
        }
        isSuccess = true;
        break;
      } catch (error) {
        wx.showToast({
          icon: 'none',
          title: `${apiType.title}请求失败，尝试下一个提供者. . .\n${error.message}` || 'Failed',
        });
        apiTypeIndex = ((apiTypeIndex + 1) % apiTypes.length) || 1;
        triedApiCount++;
      }
    }

    wx.hideLoading();

    if (!isSuccess) {
      wx.showToast({
        icon: 'none',
        title: '所有提供者请求均返回失败结果，请与作者联系。',
      });
    }
  },
  resolveData(res, key) {
    const result = res.data;
    let resultText = '';

    switch(key) {
      case 'auto':
      case 'huawei':
        const { result: { words_block_list } } = res.data;
        resultText = words_block_list.map(block => block.words).join('\n');
        break;
      case 'tencent':
        const { Response: { TextDetections } } = res.data;
        resultText = TextDetections.map(td => td.DetectedText).join('\n');
        break;
      case 'baidu':
        const { paragraphs_result, words_result } = res.data;
        resultText = paragraphs_result.map(({ words_result_idx }) => {
          return words_result_idx.map(idx => words_result[idx].words).join('')
        }).join('\n');
        break;
    }

    this.setData({
      result,
      resultText,
    });
  },
  handleBack () {
    this.setData({
      state: 0,
      imagePath: '',
      resultText: '',
      result: null
    });
  },
  async sendToPC () {
    wx.showLoading({
      title: '正在发送至PC端 . . .',
      mask: true,
    });
    const { result, baseUrl, apiTypes, apiValue } = this.data;
    const type = apiTypes[apiValue].key;
    let fullString = JSON.stringify(result);
    const datas = [];
    while(fullString.length > 4096) {
      datas.push(fullString.substring(0, 4096));
      fullString = fullString.substring(4096);
    }
    datas.push(fullString);

    const key = getUUID();
    const total = datas.length;
    const promises = datas.map((data, index) => {
      return new Promise(resolve => {
        wx.request({
          url: baseUrl,
          method: 'POST',
          data: {
            key,
            type,
            total,
            index,
            text: data,
          },
          success (res) {
            resolve();
          }
        });
      });
    });

    await Promise.all(promises);
    wx.hideLoading();
    wx.showToast({
      title: '已发送到PC！',
    });
  },
  inputHandle (e) {
    const { target: { dataset: { key } }, detail: { value } } = e;
    this.setData({
      [key]: value
    });
  },
  error(e) {
    console.log(e.detail);
  },
  handleShot (e) {
    this.setData({
      state: 1
    });
  },
  handleChoose (e) {
    wx.chooseMedia({
      count: 1,
      mediaType: ['image'],
      sourceType: ['album'],
      success: e => {
        if (this.data.noCrop) {
          this.setData({
            state: 1,
            imagePath: e.tempFiles[0].tempFilePath,
          }, this.readText);
        } else {
          this.setData({
            state: 1,
            tempPath: e.tempFiles[0].tempFilePath,
          }, this.showSizer);
        }
      },
      fail: e => {
        wx.showToast({
          title: e.message,
        });
      }
    });
  },
  handleClear (e) {
    this.setData({
      imagePath: '',
      result: null,
      resultText: ''
    });
  },
  bindPickerChange: function(e) {
    this.setData({
      apiValue: e.detail.value
    })
  },
  handleAutoUploadChange (e) {
    const values = e.detail.value;
    const val = values.includes('autoUpload');
    this.setData({
      shouldAutoUpload: val,
    });
    wx.setStorageSync(autoUploadKey, val);
  },
  handleCbgPreChange (e) {
    const values = e.detail.value;
    const val = values.includes('noCrop');
    this.setData({
      noCrop: val,
    });
    wx.setStorageSync(noCropKey, val);
  },
  showSizer() {
    this.cropper = this.selectComponent("#image-cropper");
    wx.showLoading({
      mask: true,
    });
  },
  cropperload(e){
    console.log("cropper初始化完成");
  },
  async loadimage(e){
    const { pixelRatio } = await wx.getSystemInfo();
    this.setData({
      cropMaxWidth: e.detail.width / pixelRatio,
      cropMaxHeight: e.detail.height / pixelRatio,
    });

    wx.hideLoading();
    //重置图片角度、缩放、位置
    this.cropper.imgReset();
  },
  clickcut(e) {
    console.log(e.detail);
    //点击裁剪框阅览图片
    wx.previewImage({
        current: e.detail.url, // 当前显示图片的http链接
        urls: [e.detail.url] // 需要预览的图片http链接列表
    })
  },
  handleCancelCrop() {
    this.setData({
      tempPath: '',
    });
  },
  handleCropDone(e) {
    this.setData({
      tempPath: '',
      imagePath: e.detail.url,
    }, this.readText);
  },
  handleLinkIconClick() {
    wx.setClipboardData({
      data: WEB_URL,
      success() {
        wx.showToast({
          icon: 'none',
          title: 'Web页面链接已复制！',
        });
      }
    });
  }
});
