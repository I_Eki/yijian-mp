import { getBase64FromImagePath } from '../util';
import { OCRAPIBase } from './index';

class HuaweiAPI implements OCRAPIBase {
  private token: string;

  async init() {
    return new Promise<boolean>((resolve, reject) => {
      const url = 'https://iam.cn-north-4.myhuaweicloud.com/v3/auth/tokens';
      const data = {
        auth: {
          identity: {
            methods: ['password'],
            password: {
              user: {
                name: 'MementoEki',
                password: 'eki#0354#',
                domain: {
                  name: 'MementoEki'
                }
              }
            }
          },
          scope: {
            project: {
              name: 'cn-north-4'
            }
          }
        }
      }

      wx.request({
        url,
        data,
        method: 'POST',
        success: res => {
          this.token = res.header['X-Subject-Token'];
          resolve(true);
        },
        fail() {
          resolve(false);
        }
      });
    });
  }
  read(imagePath) {
    const base64Str = getBase64FromImagePath(imagePath);
    return new Promise((resolve, reject) => {
      const url = 'https://ocr.cn-north-4.myhuaweicloud.com/v2/0ee13df72b80f4e92f9dc01df3068cfb/ocr/web-image';
      const data = {
        image: base64Str,
      }

      wx.request({
        url,
        data,
        header: {
          'X-Auth-Token': this.token,
        },
        method: 'POST',
        success(res) {
          resolve(res);
        },
        fail(error) {
          reject(error);
        }
      });
    });
  }
}

export default HuaweiAPI;
