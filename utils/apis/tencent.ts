import { formatDate, getBase64FromImagePath } from '../util';
import { OCRAPIBase } from './index';
import sha256 from 'crypto-js/sha256';
import HMAC_SHA256 from 'crypto-js/hmac-sha256';

class TencentAPI implements OCRAPIBase {
  private headers = {
    'X-TC-Action': 'GeneralAccurateOCR',
    'X-TC-Region': 'ap-guangzhou',
    'X-TC-Version': '2018-11-19',
    'X-TC-Language': 'zh-CN',
    'X-TC-Timestamp': '',
    'Host': 'ocr.tencentcloudapi.com',
    'Content-Type': 'application/json; charset=utf-8'
  };
  private token: string;
  private secretID = 'AKIDfBRSDhuqWv4wXrugCSR9bgU1IJ4XjfKi';
  private secretKey = '93i6D2buxnMefQvBBuMYT8WdU4NQDk0z';

  async init() {
    return Promise.resolve(true);
  }

  getAuthorization(data, type = 'ocr') {
    this.headers['X-TC-Timestamp'] = Date.now().toString().slice(0, 10);
    const HTTPRequestMethod = 'POST';
    const CanonicalURI = '/';
    const CanonicalQueryString = '';
    const CanonicalHeaders = `content-type:application/json; charset=utf-8\nhost:${type}.tencentcloudapi.com\n`;
    const SignedHeaders = 'content-type;host';
    const HashedRequestPayload = sha256(data).toString();
    const CanonicalRequest = [
      HTTPRequestMethod,
      CanonicalURI,
      CanonicalQueryString,
      CanonicalHeaders,
      SignedHeaders,
      HashedRequestPayload,
    ].join('\n');

    const Algorithm = 'TC3-HMAC-SHA256';
    const RequestTimestamp = this.headers['X-TC-Timestamp'];
    const RequestDate = formatDate(RequestTimestamp + '000');
    const CredentialScope = `${RequestDate}/${type}/tc3_request`;
    const HashedCanonicalRequest = sha256(CanonicalRequest).toString();
    const StringToSign = [
      Algorithm,
      RequestTimestamp,
      CredentialScope,
      HashedCanonicalRequest,
    ].join('\n');
    const SecretDate = HMAC_SHA256(RequestDate, 'TC3' + this.secretKey);
    const SecretService = HMAC_SHA256(type, SecretDate);
    const SecretSigning = HMAC_SHA256('tc3_request', SecretService);
    const Signature = HMAC_SHA256(StringToSign, SecretSigning).toString();

    const Authorization = Algorithm + ' ' + [
      `Credential=${this.secretID}/${CredentialScope}`,
      'SignedHeaders=' + SignedHeaders,
      'Signature=' + Signature,
    ].join(', ');

    return Authorization;
  }

  read(imagePath) {
    const base64Str = getBase64FromImagePath(imagePath);
    return new Promise((resolve, reject) => {
      const url = 'https://ocr.tencentcloudapi.com';
      const body = {
        ImageBase64: base64Str,
      }

      const data = JSON.stringify(body);

      const Authorization = this.getAuthorization(data);
      wx.request({
        url,
        data,
        header: {
          ...this.headers,
          Authorization,
        },
        method: 'POST',
        success(res) {
          resolve(res);
        },
        fail(error) {
          reject(error);
        }
      });
    });
  }
}

export default TencentAPI;
