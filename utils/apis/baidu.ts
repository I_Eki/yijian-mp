import { OCRAPIBase } from './index';
import { getBase64FromImagePath } from '../util';

class BaiduAPI implements OCRAPIBase {
  private accessToken: string;

  init() {
    return new Promise<boolean>((resolve, reject) => {
      let url = 'https://aip.baidubce.com/oauth/2.0/token';
      url += '?grant_type=client_credentials';
      url += '&client_id=mni0tUc7Gjt7k0xPRVW0P8oG';
      url += '&client_secret=6k7OcYmcRR9zhDL6IWPdsM3GZWPEnLFH';
      wx.request({
        url,
        method: 'GET',
        success: res => {
          const { access_token, error } = res.data as any;
          if (error) return resolve(false);
          this.accessToken = access_token;
          resolve(true);
        },
        fail () {
          resolve(false);
        }
      });
    });
  }

  read(imagePath) {
    return new Promise((resolve, reject) => {
      let url = 'https://aip.baidubce.com/rest/2.0/ocr/v1/accurate_basic';
      url += '?access_token=' + this.accessToken;
      wx.request({
        url,
        method: 'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        data: {
          image: getBase64FromImagePath(imagePath),
          paragraph: true
        },
        success: res => {
          resolve(res);
        },
        fail (e) {
          reject(e);
        }
      })
    });
  }
}

export default BaiduAPI;
