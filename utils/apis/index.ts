import BaiduAPI from './baidu';
import HuaweiAPI from './huawei';
import AliyunAPI from './aliyun';
import TencentAPI from './tencent';

interface OCRAPIBase {
  init: () => Promise<boolean>;
  read: (imagePath: string, options?: any) => any;
}

export {
  OCRAPIBase,
  BaiduAPI,
  HuaweiAPI,
  AliyunAPI,
  TencentAPI,
}
