import { OCRAPIBase } from './index';
import { getUUID, getBase64FromImagePath } from '../util';
import HMAC from 'crypto-js/hmac-sha1';
import Base64 from 'crypto-js/enc-base64';

class AliyunAPI implements OCRAPIBase {
  static getSignedUrl() {
    const params = {
      Action: 'RecognizeGeneral',
      AccessKeyId: 'LTAI5tJhAhhWGFWJMHo5txam',
      SignatureMethod: 'HMAC-SHA1',
      SignatureVersion: '1.0',
      SignatureNonce: getUUID(),
      Timestamp: (new Date()).toISOString(),
      Version: '2021-07-07',
      Format: 'JSON',
    }

    const rolledParamStr = Object.keys(params).sort().map(key => {
      const newKey = encodeURIComponent(key);
      const newValue = encodeURIComponent(params[key]);
      return `${newKey}=${newValue}`;
    }).join('&');

    const strToSign = 'GET&%2F&' + encodeURIComponent(rolledParamStr);
    const key = 'PexOSjTFo8PEw9nvpCkCFVxOLFd3Nu';
    const sha1PW = HMAC(strToSign, key + '&');
    const signature = Base64.stringify(sha1PW);

    const url = `https://ocr-api.cn-hangzhou.aliyuncs.com/?Signature=${encodeURIComponent(signature)}&${rolledParamStr}`;
    return url;
  }

  init() {
    return Promise.resolve(true)
  }
  read(imagePath) {
    const url = AliyunAPI.getSignedUrl();
    return new Promise((resolve, reject) => {
      wx.request({
        url,
        data: getBase64FromImagePath(imagePath),
        success: res => {
          resolve(res);
        },
        fail: error => reject(error)
      });
    });
  }
}

export default AliyunAPI;
